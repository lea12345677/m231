# Git Branching
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie kennen die wichtigsten GIT-Befehle.<br>Sie haben ein grundsätzliches Verständnis von *commits*<br> |

Einige Git-Befehle werden Sie nur selten benötigen. Da Git sehr viel verwendet wird, kann fast jedes Problem *gegooglelt* werden und es gibt einen *stackoverflow*-Beitrag dazu.
![googling git cherry pick](images/google_git_issue.PNG)

Die wichtigsten Befehle und am häufigsten verwendeten Befehle (die man mit der Zeit auswendig kennt):
```bash
git clone <url> <folder> # clone remote repository in folder (omit folder name will use repository name as foldername)
git add <file> # stage one or multiple files
git commit # commit all staged files
git push # push all new commits to remote repository
git pull # pull all new commits from remote repository
git checkout <branch> # change branch
```

![git purr](images/git-purr.jpg)
**Sehr empfehlenswert:** Dieser Blog Artikel [GIT PURR! Git Command Explained with Cats!](https://girliemac.com/blog/2017/12/26/git-purr/) erklärt Git Commands mit Zeichnungen von Katzen. 

Es ist sehr hilfreich, wenn man die Grundlagen von *commits* und *branches* verstanden hat, damit man Potential von Git ausschöpfen kann. Eine gute Plattform dafür ist das nachfolgende spielerische Tool. 

## 9.1. Aufgabenstellung
Mithilfe des interaktiven Tutorials [Learn Git Branching](https://learngitbranching.js.org) lernen Sie spielerisch den Umgang mit GIT. Spielen Sie die einzelnen Levels durch. 

Empfohlene Levels:
 - Local: Introduction Sequence
 - Local: Ramping Up
 - Local: Moving Work Around
 - Local: A Mixed Bag
 - Remote: Push & Pull
