# Der eigene Passwort-Manager
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  4 Lektionen |
| Ziele | Eigene Passwortverwaltungslösung eingerichtet und dokumentiert. |

Auf der Webseite von KeepassXC wird in einer Frage geklärt, weshalb jeder ein Passwortmanager verwendet soll:
<blockquote>
Password reuse and simple, easy-to-guess passwords are the <b>biggest problems</b> when using online services. If one service gets compromised (either by guessing your password or by exploiting a security vulnerability in the service's infrastructure), an attacker may gain access to all of your other accounts. But using different passwords for all websites is difficult without a way of storing them somewhere safe. Especially with arbitrary password rules for various services, it becomes increasingly hard to use both strong and diverse passwords. KeePassXC stores your passwords for you in an <b>encrypted database file</b>, so you only need to remember one master password. Of course, the security of all your services depends on the strength of your master password now, but with a sufficiently strong password, the password database should be infeasible to crack. The database is encrypted with either the industry-standard <b>AES256</b> or the <b>Twofish</b> block cipher and the master password is strengthened by a configurable number of key transformations to <b>harden it against brute force attacks</b>. Additionally, you can use a key file filled with an arbitrary number of random bytes or a YubiKey to further enhance your master key.
</blockquote>
<i>Quelle: https://keepassxc.org/docs/#faq-keepass</i>

## Austausch
**Wichtig: Schauen Sie den Link im nächsten Abschnitt NICHT an!**
Diskutieren Sie ca. 5 Minuten mit Ihren Mitlernenden über das Thema Passwortmanager. Welche Kriterien denken Sie, sind bei der Wahl des Passwortmanagers wichtig? Halten Sie drei Punkte in einer Notiz fest. 


## Vergleich
Lesen Sie das [Merkblatt Passwortmanager](https://docs.datenschutz.ch/u/d/publikationen/formulare-merkblaetter/merkblatt_passwortmanager.pdf) des Datenschutzbeauftragten des Kantons Zürichs durch. 

In diesem Dokument werden die wichtigsten Vergleichskriterien für Passwort Manager. 

Was halten Sie nun über Ihre zuvor festgehaltenen Kriterien? Sind das tatsächlich die wichtigsten Kriterien oder hat die neue Information Ihnen neue Erkenntnisse gebracht?

## Eigener Passwortmanager auswählen
Auch wenn Sie bereits einen Passwortmanager haben: Suchen Sie sich einen Passwortmanager aus und begründen Sie mithilfe von messbaren und sachlichen Kriterien Ihre Präferenz. 

### Beispiel
Definieren Sie ihre Anforderungen:

 - **Anforderung Verschlüsselungsalgorithmus:** Die Passwortdatenbank muss eine sicheren Verschlüsselungsalgorithmus (AES-256, ChaCha20 oder ähnlich).
 - **Anforderung OpenSource:** Der Quellcode des Passwortmanagers muss öffentlich einsehbar sein. 

Prüfen Sie ob der Passwortmanager Ihre Anforderungen erfüllt:

|                 | **Keepass**         | Wertung | **Lastpass** | Wertung       |
|-----------------|-------------------|---------|----------|---------------|
| Link            | [keepass.info](https://keepass.info/)   |  | [lastpass.com](https://www.lastpass.com/de)  | erfüllt       |
| Verschlüsselung | AES-256, ChaCha20 | ✓ erfüllt | AES-256  | ✓ erfüllt       |
| OpenSource      | Ja                | ✓erfüllt  | nein     | ╳ nicht erfüllt |

Definieren Sie für Ihre Entscheidung mindestens 4 Kriterien und vergleichen Sie mindestens 3 unterschiedliche Passwortmanager. 

## Passwortmanager einrichten
Sie haben sich für einen Passwortmanager entschieden. Nun ist es Zeit diesen auf Ihrem Notebook (und eventuell weiteren Geräten) einzurichten. Machen Sie sich zudem folgende Überlegungen:

 - Was tun Sie, wenn Sie ihr Master-Passwort vergessen?
 - Wie erstellen Sie ein Backup von Ihrer Passwortdatenbank?<br>*Wenn Sie ein Cloud Dienst verwenden: Was tun Sie, wenn Sie keinen Zugriff auf Ihren Account mehr haben?*

## Hinweis
Diese Aufgabe ist Teil der [Leistungsbeurteilung LB3](../99_Leistungsbeurteilung/). 